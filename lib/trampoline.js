function bounce( func ) {  
  let result = func.apply(
    null,
    Array.prototype.slice.call( arguments, 1 ) )
  while ( !result.done ) {
    result = result.func()
  }
  return result.value
}

module.exports = {  
  done: function ( value ) {
    return {value: value, done: true}
  },
  continue: function ( func ) {
    return {func: func, done: false}
  },
  trampoline: function ( func ) {
    return bounce.bind( null, func )
  },
}