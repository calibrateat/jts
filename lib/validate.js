const Ajv = require( "ajv" )
const ajv = new Ajv

let schemaBasePath = "./schema/"

let validateAjv = ajv
  .addSchema( require( `${schemaBasePath}process/engine/pdf_t_b.json` ) )
  .addSchema( require( `${schemaBasePath}resource/index.json` ) )
  .addSchema( require( `${schemaBasePath}process/preview.json` ) )
  .addSchema( require( `${schemaBasePath}process/preflight.json` ) )
  .addSchema( require( `${schemaBasePath}process/color_conversion.json` ) )
  .addSchema( require( `${schemaBasePath}process/merge.json` ) )
  .addSchema( require( `${schemaBasePath}process/proof.json` ) )
  .addSchema( require( `${schemaBasePath}process/web_preview.json` ) )
  .addSchema( require( `${schemaBasePath}process/thumbnail.json` ) )
  .addSchema( require( `${schemaBasePath}process/adjust_pageboxes.json` ) )
  .compile( require( `${schemaBasePath}job_ticket.json` ) )

module.exports = validate

function validate( jobTicket, callback ) {
  let _valid = validateAjv( jobTicket )
  callback( validateAjv.errors, _valid )
}
