
const _varNameRegExp = /.*(?=\$)/

module.exports = {
  "copyObj": copyObj,
  "lastPart": varPart,
  "capitalizeFirstChar": capitalizeFirstChar,
  "idClean": idClean,
  "objDeepCheck": objDeepCheck,
  "indexNameTable": indexNameTable,
  "trampoline": trampoline,
}
/**
 * Clean a object from property 'id'
 * @param {object} obj 
 * @param {string} prop
 */
function idClean( obj, prop ){
  let _toClean = prop || "id"
  if ( typeof obj === "object" ){
    if ( Array.isArray( obj ) ){
      for ( let _elm of obj ){
        idClean( _elm, prop )
      }
    } else {
      for ( let _prop in obj ){
        if ( _prop === _toClean ){
          delete obj[_prop]
        } else {
          idClean( obj[_prop], prop )
        }
      }
    }
  }
  return obj
}
/**
 * Copy a object
 * @param {*} obj 
 * @param {*} targetObj 
 */
function copyObj( obj, targetObj ) {
  targetObj = targetObj || {}
  for ( let key in obj ) targetObj[key] = obj[key]
  // return Object.assign(targetObj, obj);
  return targetObj
}
/**
 *
 * @param {string} str
 */
function varPart( str ) {
  return str.match( _varNameRegExp )
}
/**
 * Capitalize first char
 * @param {string} str
 */
function capitalizeFirstChar( str ) {
  return str[0].toUpperCase() + str.slice( 1 )
}
/**
 * Deep property check for objects
 * @param {object} obj 
 * @param {string} deepPath 
 */
function objDeepCheck( obj, deepPath ){
  let _args = deepPath.split( "." )
  for ( let i = 0, len=_args.length; i < len; i++ ) {
    if ( !obj || !obj.hasOwnProperty( _args[i]) ) {
      return false
    }
    obj = obj[_args[i]]
  }
  return true
}

function indexNameTable ( arr,nameProp ){
  let _obj = {}
  arr.find( ( element,index )=>{
    if ( element.hasOwnProperty( nameProp ) ){
      /* Object.defineProperty(_obj, element[nameProp], {
        value: index,
        writable: true,
        enumerable: true,
      }) */
      /* Object.defineProperty(_obj, element[nameProp], {
        get() { return index; },
        enumerable: true,
        configurable: true,
      }); */
      _obj[element[nameProp]] = index
    }
  })
  return _obj
}

function trampoline( fn ) {
  while ( fn && fn instanceof Function ) {
    fn = fn()
  }
  return fn
}

// test
/* let _testObj = {"foo": [{"foo": {"id": "apple"}}],"id": "pear"}

console.log( JSON.stringify( idClean( _testObj ) ) ) */

/* let _arr = [{"bar":"zuzu","foo":45},{"bar":"rtz","foo":45},{"bar":"retrcg","foo":45},{"bar":"bgf","foo":45},{"bar":"bgf","foo":66}]
let _table = indexNameTable( _arr, "bar" )
console.log( JSON.stringify( _table ) ) */