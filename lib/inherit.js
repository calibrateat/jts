const objectID = require( "mongodb" ).ObjectID
const defaultSetting = {"id": "id"}

module.exports = inherit

/**
 * Inherit all not unknown properties in child from parent
 * @param {object} child
 * @param {object} parent
 * @param {object} setting
 */
function inherit( child, parent, setting ) {
  // setting = setting || defaultSetting
  const args = Array.from( arguments )
  args.shift()
  setting = args.pop()
  let _parentList
  if ( setting.reverse ) {
    _parentList = args.reverse()
  } else {
    _parentList = args
  }
  let _parentCount = _parentList.length
  if ( _parentCount < 1 ) {
    return child
  }
  let _parents = _parentList[0]
  for ( let _parentIndex = 1; _parentIndex < _parentCount; _parentIndex++ ) {
    // if (typeof _parent === 'object') {
    _parents = objectAssign( _parents, _parentList[_parentIndex], setting )
  }
  // for (let _parent of _parents) {
  //  if (typeof _parent === 'object') {
  objectAssign( child, _parents, setting )
  //  }
  // }
  return child
}
/**
 * Assign Objects
 * If Array/List contain object, they need a unique is
 * @param {object} child
 * @param {object} parent
 * @param {object} setting
 */
function objectAssign( child, parent, setting ) {
  if ( parent instanceof Array ) {
    parent.find( ( element, index, array ) => {
      if ( typeof element !== "object" ) {
        if ( !child.includes( element ) ) {
          child.unshift( element )
        }
      } else {
        let _found = false
        for ( let _value of child ) {
          if ( typeof _value === "object" ) {
            let _id = hasUniqueKey( _value, setting )
            if ( !_id ) {
              _id = ( setting.id.match( /,/ ) ) ? setting.id.match( /^.+(?=,)/ ) : setting.id
              _value[_id] = new objectID().toString()
            }
            let _childId = _value[_id]
            let _elementId = element[_id]
            if ( _childId === _elementId ) {
              _found = true
              _value = objectAssign( _value, element, setting )
              break
            }
          }
        }
        if ( !_found ) {
          child.unshift( element )
        }
      }
    })
  } else {
    for ( let _property in parent ) {
      if ( !child.hasOwnProperty( _property ) ) {
        // if (_property !== setting.id) {
        child[_property] = parent[_property]
        // }
      } else {
        // iterate only over objects
        if (typeof child[_property] === 'object' && child[_property] !== null) {
          if ( !Object.is( child[_property], parent[_property]) ) {
            child[_property] = objectAssign( child[_property], parent[_property], setting )
          }
        }
      }
    }
  }
  return child
}

function hasUniqueKey( checkObj, setting ) {
  if ( !setting.id ) { setting.id = defaultSetting.id }
  let _idArray = setting.id.split( "," ).map( ( item ) => {
    return item.trim()
  })
  if ( _idArray.length > 1 ) {
    for ( let _id of _idArray ) {
      if ( checkObj[_id]) {
        return _id
      }
    }
  } else {
    if ( checkObj[setting.id]) {
      return setting.id
    }
  }
  return false
}


// test
/*
console.log('----------- inherit test start -----------')
// let _child = {'foo': 'jjjjj', 'fooBar': [{'qux': 123}], 'bar': {'foo': ['banana', 'apple']}}
// let _parent = {'foo': 'kkkkk', 'barFoo': 'schnaps', 'qux': ['beer', 'wine'], 'bar': {'foo': ['strawberry']}, 'fooBar': [{'qux': 345}, 'hello']}
let _child = {
  'base': [
    'c', 'd',
  ],
  'checks': [
    {
      'key': 'quxFoo',
      'severity': 2,
    },
  ],
}
let _parent_001 = {
  'id': 'a',
  'type': 'standard',
  'base': [],
  'name': 'PDF/X requirements',
  'checks': [
    {
      'key': 'a',
    },
    {
      'key': 'foo',
      'severity': 3,
    },
    {
      'key': 'qux',
      'severity': 1,
    },
  ],
  'fixups': [],
}
let _parent_002 = {
  'id': 'b',
  'type': 'standard',
  'base': ['d'],
  'name': 'PDF/X requirements',
  'checks': [
    {
      'key': 'b',
    },
    {
      'key': 'foo',
      'severity': 2,
    },
    {
      'key': 'quxFoo',
      'severity': 1,
    },
  ],
  'fixups': [],
}
let _parent_003 = {
  'id': 'c',
  'type': 'standard',
  'base': ['d'],
  'name': 'PDF/X requirements',
  'checks': [
    {
      'key': 'c',
    },
    {
      'key': 'foo',
      'severity': 1,
    },
    {
      'key': 'quxBar',
      'severity': 1,
    },
  ],
  'fixups': [],
}

console.log(JSON.stringify(inherit(_child, _parent_002, _parent_001, _parent_003, {'id': 'id, key', 'reverse': true}))) */
