// --------------------------------------------- //
//                                               //
//         JTS - Job Ticket Service              //
//                  by calibrate.at              //
//                                               //
// --------------------------------------------- //

let jcs = require('jcs')
let validate = require('./lib/validate')
let inherit = require('./lib/inherit')
let util = require('./lib/util')
let taskConverter = require('./lib/task_converter')
// let trampoline = require( "./lib/trampoline" )

module.exports = Jts

function Jts (jobTicket, options) {
  this._options = options || {}
  this._options.templateOnly = this._options.templateOnly || false;
  // toDo QuickCheck
  // Validate jobticket
  if (options.validate) {
    validate(jobTicket, (err, data) => {
      if (err) {
        console.log(JSON.stringify(err))
        return null
      }
    })
  }
  // General Option
  let _generalAdd = jcs(options.generalCondition, jobTicket)
  // jobTicket = inherit.call(this, [].concat([jobTicket], _generalAdd, [{'id': 'id'}]))
  _generalAdd.reverse()
  // console.log( `Matched General Conditons: ${JSON.stringify( _generalAdd )}` )
  for (let _extends of _generalAdd) {
    jobTicket = inherit.call(this, jobTicket, _extends.data, { 'id': 'id, key' })
  }

  // console.log( `After generalcondition: ${JSON.stringify( jobTicket )}` )
  // Process Templates
  if (jobTicket.processList) {
    for (let _process of jobTicket.processList) {
      let _templateList = _process.templates
      if (_templateList) {
        _templateList.reverse()
        for (let _id of _templateList) {
          let _processTemplate = options.processCollection.find((template) => { return template.id === _id })
          if (_processTemplate) {
            // console.log( JSON.stringify( _processTemplate ) )
            console.log(`Template with id: ${_id} found`)
            _process = inherit.call(this, _process, _processTemplate, { 'id': 'id, key' })
            // console.log( JSON.stringify( _process ) )
          } else {
            console.log(`Template with id: ${_id} not found`)
          }
        }
      }
      // Process Engine Templates
      let _processTemplateListProperty = _process.type
      if (_process.processVars) {
        let _processVars = _process.processVars
        if (!_processVars.engine) {
          _processVars.engine = 'pdfTB'
        }
        if (_processVars.template) {
          let _processVarsTemplate = _processVars.template
          let _templateId = _processVarsTemplate.id
          _processTemplateListProperty += `${util.capitalizeFirstChar(_processVars.engine)}$collection`
          let _processVarsTemplateVaseCollection = options[_processTemplateListProperty]
          if (_processVarsTemplateVaseCollection) {
            // _processVars.BasesTemplate = baseIterateInherit.call( this, _processVarsTemplate, _processVarsTemplateVaseCollection )
            _processVars.BasesTemplate = baseInherit.call(this, _processVarsTemplate, _processVarsTemplateVaseCollection)
            _processVars.template = inherit.call(this, _processVars.template, _processVars.BasesTemplate, { 'id': 'id, key' })
            console.log(`after template inherit: ${JSON.stringify(_processVars.template)}`)
            if (_templateId) {
              _processVars.template.id = _templateId
            }
          } else {
            _processVars.template.id = 'gerneratedTemplate'
          }
          _processVars.template.base = []
        } else {
          console.log(`No templateCollection with name: ${_processTemplateListProperty}found `)
        }
      }
    }
  }
  let _task = (this._options.templateOnly) ? jobTicket.processList[0].processVars.template : taskConverter.call(this, jobTicket);
  // console.log(JSON.stringify(_task))
  return _task
}
/**
 *
 * @param {object} baseElement
 * @param {object} baseCollection
 */
/* function baseIterateInherit( baseElement, baseCollection ) {
  console.log( `Base: ${  baseElement.base}` )
  if ( typeof baseElement.base !== "undefined" && baseElement.base.length > 0 ) {
    let _base = baseElement.base
    let _baseCount = _base.length
    let _baseTemplates = []
    for ( let _templateId of _base ) {
      let _found = false
      baseCollection.find( ( element, index, array ) => {
        if ( element.id === _templateId ) {
          _found = true
          let _parent = element
          if ( ( typeof element.base !== "undefined" && element.base.length > 0 ) ) {
            element = baseIterateInherit.call( this, element, baseCollection )
            element = inherit.call( this, element, _parent, {"id": "id, key"})
          }
          _baseTemplates.push( element )
          return true
        }
        return false
      }, this )
      if ( !_found ) {
        console.log( `No template for base elemement id: ${_templateId} found` )
      }
    }
    // console.log( `BaseTemplates: ${  JSON.stringify( _baseTemplates )}` )
    let _count = _baseTemplates.length
    if ( _baseTemplates.length ) {
      let _inheritObj = _baseTemplates[0]
      if ( _baseTemplates.length > 1 ) {
        _baseTemplates = _baseTemplates.reverse()
        _inheritObj = inherit.apply( this, _baseTemplates.concat([{"id": "id, key"}]) )
      }
      return _inheritObj
    }
  }
  return baseElement
} */
/**
 * Inherit and iterate baseList
 * @param {object} baseElement
 * @param {array} baseCollection
 * @returns {object}
 */
function baseInherit (baseElement, baseCollection) {
  this._indexTable = util.indexNameTable(baseCollection, 'id')
  this._collection = baseCollection
  let _baseList = baseElement.base
  // console.log( _baseList )
  if (typeof _baseList !== 'undefined' && _baseList.length > 0) {
    let _baseTemplates = baseString.call(this, _baseList.toString())
    // console.log(_baseTemplates)
    let _baseArr = _baseTemplates.split(',')
    _baseArr.reverse()
    baseElement = {}
    // for (let _i=0,_len=_baseArr.length; _i<_len; _i++){
    for (let _id of _baseArr) {
      let _index = this._indexTable[_id]
      let _parent = this._collection[_index]
      baseElement = inherit.call(this, baseElement, _parent, { 'id': 'id, key' })
    }
    baseElement.base = []
  }
  delete this._indexTable
  delete this._collection
  return baseElement
}
/**
 * Read and iterate over baselist
 * @param {string} baseList
 * @returns {string}
 */
function baseString (baseList) {
  let recur = function (result, baseList) {
    let _unresolved = [];
    if (baseList === '') {
      return result
    }
    let _baseArr = baseList.split(',')
    let _result = ''
    for (let _id of _baseArr) {
      console.log( "Resolving cmp Template id: " + _id )
      let _index = this._indexTable[_id]
      if (_index === undefined) {
        throw new Error (`Template with id ${_id} not found`);
        break;
      }
      let _baseList = baseByIndex.call(this, _index)
      // let _result = ""
      if (typeof _baseList !== 'undefined' && _baseList.length > 0) {
        let _resString = util.trampoline(recur.bind(this, _result, _baseList))
        _result += `,${_resString}`
      }
      _result += `,${_id}`
    }
    _result = _result.replace(/^,/, '')
    // console.log("Result: " + _result)
    return _result
  }
  return util.trampoline(recur.call(this, '', baseList))
}
/**
 *  Return a given baseListString
 *  or empty string
 * @param {number} index
 * @returns {string}
 */
function baseByIndex (index) {
  let _baseList = this._collection[index].base
  if (_baseList !== undefined && _baseList.length > 0) {
    return _baseList.toString()
  }
  return ''
}
