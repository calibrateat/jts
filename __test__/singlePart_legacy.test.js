const Jts = require( "../index.js" )
const fs = require("fs");
const ObjectID = require( "mongodb" ).ObjectID

let _options = {
  "generalCondition": require( "./calibrate/general_conditions_legacy.json" ),
  "processCollection": require( "./calibrate/jobticket_process_templates.json" ),
  "preflightPdfTB$collection": require( "./calibrate/templates.json" ).concat(require('./calibrate/custom.json')),
  "processDataCollection": require( "./calibrate/task_process_templates.json" ),
  "validate": true,
}

// test
ObjectID.toString = jest.fn(() => 1482363367071);
ObjectID.toHexString = jest.fn(() => 1482363367071);


test("jts test", () => {
  let _jobTicket = JSON.parse(fs.readFileSync("./__test__/calibrateTickets/ticket_001.json", "utf8"));
  let jts = new Jts( _jobTicket, _options )
  expect(JSON.parse(jts.processes[0].data.variables[2].pdf_t_b_template)).toMatchSnapshot();
});