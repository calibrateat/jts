# JTS - Job Ticket Service
Service for mapping a calibrate jobticket to a ppa Task.
## options
this service need a number of options, really need.
## sample
let _options = {
  "generalCondition": require( "./condition/general_condion_001.json" ),
  "processCollection": require( "./collection/process_template_demo.json" ),
  "preflightPdfTB$collection": require( "./collection/pdf_t_b_templates_demo.json" ),
  "processDataCollection": require( "./collection/process_preflight_data_collection.json" ),
  "cbUrl": "http://customer.callback.at",
  "validate": true,
}

// see test.js there is a working sample

const Jts = require( "jts" )

// Files are found in test folder

let _options = {
  "generalCondition": require( "./condition/general_condion_001.json" ),
  "processCollection": require( "./collection/process_template_demo.json" ),
  "preflightPdfTB$collection": require( "./collection/pdf_t_b_templates_demo.json" ),
  "processDataCollection": require( "./collection/process_preflight_data_collection.json" ),
  "cbUrl": "http://customer.callback.at",
  "validate": true,
}

// the income jobticket

let _jobTicket = require( "./ticket_001.json" )

// task generator

let newTask = new Jts( _jobTicket, _options )
