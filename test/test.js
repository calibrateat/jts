const Jts = require( "../index.js" )

let _options = {
  "generalCondition": require( "./geigerNotes/general_condion.json" ),
  "processCollection": require( "./geigerNotes/process_template.json" ),
  "preflightPdfTB$collection": require( "./geigerNotes/pdf_t_b_templates.json" ),
  "processDataCollection": require( "./geigerNotes/process_preflight_data_collection.json" ),
  "validate": true,
}

// test
console.log( "JTS test startet" )
let _jobTicket = require( "./ticket_001.json" )
let jts = new Jts( _jobTicket, _options )

console.log( JSON.stringify( jts ) )

/* const util = require( "../lib/util" )
const inherit = require( "../lib/inherit" )

let _template = require( "./collection/pdf_t_b_templates_demo.json" )
let _baseElement = {
  "base": [
    "c", "d",
  ],
  "checks": [
    {
      "id": "pageCountExact",
      "severity": 3,
      "variables": [
        {
          "key": "count",
          "value": 10,
        },
      ],
    },
    {
      "id": "pageSizeUnequal",
      "severity": 3,
      "variables": [
        {
          "key": "width",
          "value": 210,
        },
        {
          "key": "height",
          "value": 297,
        },
      ],
    },
  ],
} */
/**
 * Inherit and iterate baseList 
 * @param {object} baseElement 
 * @param {array} baseCollection
 * @returns {object}
 */
/* function baseInherit( baseElement, baseCollection ) {
  this._indexTable = util.indexNameTable( baseCollection, "id" )
  this._collection = baseCollection
  let _baseList = baseElement.base
  console.log( _baseList )
  if ( typeof _baseList !== "undefined" && _baseList.length > 0 ) {
    let _baseTemplates = baseString.call( this, _baseList.toString() )
    //console.log(_baseTemplates)
    let _baseArr = _baseTemplates.split( "," )
    _baseArr.reverse()
    baseElement = {}
    // for (let _i=0,_len=_baseArr.length; _i<_len; _i++){
    for ( let _id of _baseArr ) {
      let _index = _indexTable[_id]
      let _parent = _collection[_index]
      baseElement = inherit.call( this, baseElement, _parent, {"id": "id, key"})
    }
    baseElement.base = []
  }
  return baseElement
} */
/**
 * Read and iterate over baselist
 * @param {string} baseList
 * @returns {string}
 */
/* function baseString( baseList ) {
  let recur = function( result, baseList ) {
    if ( baseList === "" ) {
      return result
    }
    let _baseArr = baseList.split( "," )
    let _result = ""
    for ( let _id of _baseArr ) {
      // console.log( "Current id: " + _id )
      let _index = _indexTable[_id]
      if ( _index === undefined ) {
        break
      }
      let _baseList = baseByIndex.call( this, _index )
      // let _result = ""
      if ( typeof _baseList !== "undefined" && _baseList.length > 0 ) {
        let _resString = util.trampoline( recur.bind( this, _result, _baseList ) )
        _result += `,${  _resString}`
      }
      _result += `,${  _id}`
    }
    _result = _result.replace( /^,/, "" )
    // console.log("Result: " + _result)
    return _result
  }
  return util.trampoline( recur.call( this, "", baseList ) )
} */
/**
 *  Return a given baseListString
 *  or empty string
 * @param {number} index
 * @returns {string}
 */
/* function baseByIndex( index ) {
  let _baseList = this._collection[index].base
  if ( _baseList !== undefined && _baseList.length > 0 ) {
    return _baseList.toString()
  }
  return ""
} */

/* let _result = baseInherit( _baseElement, _template )

console.log( JSON.stringify( _result ) ) */