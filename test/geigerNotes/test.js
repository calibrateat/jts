const Jts = require( "../../index.js" )

let _options = {
  "generalCondition": require( "./general_condition.json" ),
  "processCollection": require( "./process_templates.json" ),
  "preflightPdfTB$collection": require( "./custom.json" ).concat(require('./templates.json')),
  "processDataCollection": require( "./task_process_templates.json" ),
  
  "validate": false,
  "templateOnly": true
}
// option."cbUrl": "http://customer.callback.at",
// test

console.log( "JTS test startet" )
let _jobTicket = require( "./ticket_004.json" )
let jts = new Jts( _jobTicket, _options )

console.log( JSON.stringify( jts ) )