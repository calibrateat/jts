const Jts = require( "../../index.js" )

let _options = {
  "generalCondition": require( "./geigerNotes/general_condion_geigerNotes.json" ),
  "processCollection": require( "./geigerNotes/process_template.json" ),
  "preflightPdfTB$collection": require( "./geigerNotes/templates_geigerNotes.json" ),
  "processDataCollection": require( "./geigerNotes/process_preflight_data_collection.json" ),
  "cbUrl": "http://customer.callback.at",
  "validate": true,
}

// test
console.log( "JTS test startet" )
let _jobTicket = require( "./ticket_003.json" )
let jts = new Jts( _jobTicket, _options )

console.log( JSON.stringify( jts ) )