"use strict"

module.exports = {
  "generalCondition": require( "./geigerNotes/general_condion_geigerNotes.json" ),
  "processCollection": require( "./geigerNotes/process_template.json" ),
  "preflightPdfTB$collection": require( "./geigerNotes/templates_geigerNotes.json" ),
  "processDataCollection": require( "./geigerNotes/process_preflight_data_collection.json" ),
  "cbUrl": "http://customer.callback.at",
  "validate": true,
}
